#!/bin/sh
#
# This file is part of crossroad.
# Copyright (C) 2013 Jehan <jehan at girinstud.io>
#
# crossroad is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# crossroad is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with crossroad.  If not, see <http://www.gnu.org/licenses/>.

# Some value for user usage.
export CROSSROAD_BUILD=`@DATADIR@/share/crossroad/scripts/config.guess`
export CROSSROAD_CMAKE_TOOLCHAIN_FILE="@DATADIR@/share/crossroad/scripts/cmake/toolchain-${CROSSROAD_PLATFORM}.cmake"
export CROSSROAD_MESON_TOOLCHAIN_FILE="@DATADIR@/share/crossroad/scripts/meson/toolchain-${CROSSROAD_PLATFORM}.meson"

if [ -f "$CROSSROAD_HOME/.depends.crossroad" ]; then
  export CROSSROAD_DEPENDENCIES=$(<"$CROSSROAD_HOME/.depends.crossroad")
else
  export CROSSROAD_DEPENDENCIES=
fi
if [ -f "$CROSSROAD_HOME/.tools.crossroad" ]; then
  export CROSSROAD_TOOLS=$(<"$CROSSROAD_HOME/.tools.crossroad")
else
  export CROSSROAD_TOOLS=
fi

# Compute the platform word-size for the "native" platform.
LONG_BIT=`getconf LONG_BIT`
if [ x"$CROSSROAD_PLATFORM" = x"native" ]; then
  if [ x"$LONG_BIT" = x"32" ] || \
     [ x"$LONG_BIT" = x"64" ]; then
    export CROSSROAD_WORD_SIZE=$LONG_BIT
  fi
fi

if [ x"$CROSSROAD_PLATFORM" = x"native" ]; then
  GCC="gcc"
else
  GCC="${CROSSROAD_HOST}-gcc"
fi

DEPS_LD_LIBRARY_PATH=
DEPS_GI_TYPELIB_PATH=
while IFS= read -r dep;
do
  if [ -n "$dep" ]; then
    dep_prefix="$CROSSROAD_PREFIX/../$dep"

    export PATH="$dep_prefix/bin:$PATH"

    if [ -n "$DEPS_LD_LIBRARY_PATH" ]; then
      DEPS_LD_LIBRARY_PATH=$dep_prefix/lib:$DEPS_LD_LIBRARY_PATH
    else
      DEPS_LD_LIBRARY_PATH=$dep_prefix/lib
    fi
    if [ -n "$DEPS_XDG_DATA_DIRS" ]; then
      DEPS_XDG_DATA_DIRS="$dep_prefix/share:$DEPS_XDG_DATA_DIRS"
    else
      DEPS_XDG_DATA_DIRS="$dep_prefix/share"
    fi

    if [ -n "$DEPS_GI_TYPELIB_PATH" ]; then
      DEPS_GI_TYPELIB_PATH=$dep_prefix/lib/girepository-1.0/:$DEPS_GI_TYPELIB_PATH
    else
      DEPS_GI_TYPELIB_PATH=$dep_prefix/lib/girepository-1.0/
    fi

    if [ "x$CROSSROAD_WORD_SIZE" != "x" ]; then
      DEPS_LD_LIBRARY_PATH=$dep_prefix/lib${CROSSROAD_WORD_SIZE}:$DEPS_LD_LIBRARY_PATH
      DEPS_GI_TYPELIB_PATH=$dep_prefix/lib${CROSSROAD_WORD_SIZE}/girepository-1.0/:$DEPS_GI_TYPELIB_PATH
    fi

    if [ "x`$GCC -print-multiarch`" != "x" ]; then
      DEPS_LD_LIBRARY_PATH=$dep_prefix/lib/`$GCC -print-multiarch`:$DEPS_LD_LIBRARY_PATH
      DEPS_GI_TYPELIB_PATH=$dep_prefix/lib/`$GCC -print-multiarch`/girepository-1.0/:$DEPS_GI_TYPELIB_PATH
    fi

    if [ x"$CROSSROAD_PLATFORM" = x"native" ]; then
      PKG_CONFIG_PATH=$dep_prefix/lib/pkgconfig:$dep_prefix/share/pkgconfig:$PKG_CONFIG_PATH
      if [ "x$CROSSROAD_WORD_SIZE" != "x" ]; then
        PKG_CONFIG_PATH=$dep_prefix/lib${CROSSROAD_WORD_SIZE}/pkgconfig:$PKG_CONFIG_PATH
      fi
      if [ "x`$GCC -print-multiarch`" != "x" ]; then
        PKG_CONFIG_PATH=$dep_prefix/lib/`$GCC -print-multiarch`/pkgconfig/:$PKG_CONFIG_PATH
      fi

      for dir in $(find $dep_prefix/lib/ $dep_prefix/lib${CROSSROAD_WORD_SIZE}/ -maxdepth 1 -name 'python3.*' 2>/dev/null);
      do
        export PYTHONPATH=${dir}/site-packages:$PYTHONPATH
      done;
      export PYTHONPATH=$dep_prefix/share/glib-2.0/:$PYTHONPATH
    fi

    unset dep_prefix
  fi
done <<< "$CROSSROAD_DEPENDENCIES";

TOOLS_PATH=
TOOLS_PYTHONPATH=
TOOLS_LD_LIBRARY_PATH=
TOOLS_GI_TYPELIB_PATH=
rec_update_tools() {
  tool_project_name="$1"
  if [ -n "$tool_project_name" ]; then
    tools_prefix="$CROSSROAD_PREFIX/../../native/$tool_project_name"

    if [ -n "$TOOLS_PATH" ]; then
      TOOLS_PATH="$TOOLS_PATH:$tools_prefix/bin"
    else
      TOOLS_PATH="$tools_prefix/bin"
    fi

    ld_libs="$tools_prefix/lib"
    gi_typelibs="$tools_prefix/lib/girepository-1.0/"
    if [ "x$LONG_BIT" != "x" ]; then
      ld_libs=$tools_prefix/lib${LONG_BIT}:$ld_libs
      gi_typelibs=$tools_prefix/lib${LONG_BIT}/girepository-1.0/:$gi_typelibs
    fi
    if [ "x`cc -print-multiarch`" != "x" ]; then
      ld_libs=$tools_prefix/lib/`cc -print-multiarch`:$ld_libs
      gi_typelibs=$tools_prefix/lib/`$GCC -print-multiarch`/girepository-1.0/:$gi_typelibs
    fi
    if [ -n "$TOOLS_LD_LIBRARY_PATH" ]; then
      TOOLS_LD_LIBRARY_PATH="$TOOLS_LD_LIBRARY_PATH:$ld_libs"
    else
      TOOLS_LD_LIBRARY_PATH="$ld_libs"
    fi
    if [ -n "$TOOLS_GI_TYPELIB_PATH" ]; then
      TOOLS_GI_TYPELIB_PATH="$TOOLS_GI_TYPELIB_PATH:$gi_typelibs"
    else
      TOOLS_GI_TYPELIB_PATH="$gi_typelibs"
    fi
    unset ld_libs
    unset gi_typelibs

    if [ -n "$TOOLS_PYTHONPATH" ]; then
      export TOOLS_PYTHONPATH="$tools_prefix/share/glib-2.0/"
    else
      export TOOLS_PYTHONPATH="$TOOLS_PYTHONPATH:$tools_prefix/share/glib-2.0/"
    fi
    for dir in $(find $tools_prefix/lib/ $tools_prefix/lib${LONG_BIT}/ -maxdepth 1 -name 'python3.*' 2>/dev/null);
    do
      export TOOLS_PYTHONPATH="$TOOLS_PYTHONPATH:${dir}/site-packages"
    done;

    unset tools_prefix

    tool_project_home="$CROSSROAD_HOME/../../native/$tool_project_name"
    if [ -f "$tool_project_home/.depends.crossroad" ]; then
      tool_dependencies=$(<"$tool_project_home/.depends.crossroad")

      while IFS= read -r tool_dep_name;
      do
        rec_update_tools "$tool_dep_name"
      done <<< "$tool_dependencies";

      unset tool_dependencies
    fi
    unset tool_project_home
  fi
  unset tool_project_name
}

while IFS= read -r tool_project;
do
  rec_update_tools "$tool_project"
done <<< "$CROSSROAD_TOOLS";

if [ "x$TOOLS_PATH" != "x" ]; then
  export PATH=$TOOLS_PATH:$PATH
fi
if [ "x$TOOLS_PYTHONPATH" != "x" ]; then
  export PYTHONPATH=$TOOLS_PYTHONPATH:$PYTHONPATH
fi

export PATH="$CROSSROAD_PREFIX/bin:$PATH"

if [ x"$CROSSROAD_PLATFORM" = x"native" ]; then
  if [ -n "$DEPS_LD_LIBRARY_PATH" ]; then
    DEPS_LD_LIBRARY_PATH=":$DEPS_LD_LIBRARY_PATH:$LD_LIBRARY_PATH"
    DEPS_GI_TYPELIB_PATH=":$DEPS_GI_TYPELIB_PATH:$GI_TYPELIB_PATH"
  else
    DEPS_LD_LIBRARY_PATH=":$LD_LIBRARY_PATH"
    DEPS_GI_TYPELIB_PATH=":$GI_TYPELIB_PATH"
  fi
else
  if [ -n "$DEPS_LD_LIBRARY_PATH" ]; then
    DEPS_LD_LIBRARY_PATH=":$DEPS_LD_LIBRARY_PATH"
    DEPS_GI_TYPELIB_PATH=":$DEPS_GI_TYPELIB_PATH"
  fi
fi

export LD_LIBRARY_PATH="$CROSSROAD_PREFIX/lib$DEPS_LD_LIBRARY_PATH"
if [ x"$CROSSROAD_PLATFORM" = x"native" ]; then
  export GI_TYPELIB_PATH="$CROSSROAD_PREFIX/lib/girepository-1.0/$DEPS_GI_TYPELIB_PATH"
else
  unset GI_TYPELIB_PATH
fi

unset DEPS_LD_LIBRARY_PATH
unset DEPS_GI_TYPELIB_PATH

if [ "x$TOOLS_LD_LIBRARY_PATH" != "x" ]; then
  export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$TOOLS_LD_LIBRARY_PATH"
fi
if [ "x$TOOLS_GI_TYPELIB_PATH" != "x" ]; then
  if [ -n "$GI_TYPELIB_PATH" ]; then
    export GI_TYPELIB_PATH="$GI_TYPELIB_PATH:$TOOLS_GI_TYPELIB_PATH"
  else
    export GI_TYPELIB_PATH="$TOOLS_GI_TYPELIB_PATH"
  fi
fi

if [ "x$CROSSROAD_WORD_SIZE" != "x" ]; then
  export LD_LIBRARY_PATH=$CROSSROAD_PREFIX/lib${CROSSROAD_WORD_SIZE}:$LD_LIBRARY_PATH
  if [ x"$CROSSROAD_PLATFORM" = x"native" ]; then
    export GI_TYPELIB_PATH=$CROSSROAD_PREFIX/lib${CROSSROAD_WORD_SIZE}/girepository-1.0/:$GI_TYPELIB_PATH
  fi
fi

# This relies on GCC. Clang/LLVM probably has an equivalent to find
# specific multi-arch paths but I don't know about it.
# This is needed because I realized that on some distributions, for
# instance Debian, libraries were installed in lib/x86_64-linux-gnu/.
# And it would seem that some build systems (like meson) would detect
# the distribution standard paths and install the same in custom
# prefixes. So we need to add these paths as well.
if [ "x`$GCC -print-multiarch`" != "x" ]; then
  export LD_LIBRARY_PATH=$CROSSROAD_PREFIX/lib/`$GCC -print-multiarch`:$LD_LIBRARY_PATH
  export GI_TYPELIB_PATH=$CROSSROAD_PREFIX/lib/`$GCC -print-multiarch`/girepository-1.0/:$GI_TYPELIB_PATH
fi

if [ x"$CROSSROAD_PLATFORM" = x"w32" ] || \
   [ x"$CROSSROAD_PLATFORM" = x"w64" ]; then
  # ld is a mandatory file to enter this environment.
  # Also it is normally not touched by ccache, which makes it a better
  # prefix-searching tool than gcc.
  host_ld="`which $CROSSROAD_HOST-ld`"
  host_ld_dir="`dirname $host_ld`"
  host_ld_bin="`basename $host_ld_dir`"

  if [ $host_ld_bin = "bin" ]; then
      host_ld_prefix="`dirname $host_ld_dir`"
      # No need to add the guessed prefix if it is a common one that we add anyway.
      if [ "$host_ld_prefix" != "/usr" ]; then
          if [ "$host_ld_prefix" != "/usr/local" ]; then
              if [ -d "$host_ld_prefix/$CROSSROAD_HOST" ]; then
                  export CROSSROAD_GUESSED_MINGW_PREFIX="$host_ld_prefix/$CROSSROAD_HOST"
              fi
          fi
      fi
      unset host_ld_prefix
  fi
  unset host_ld_bin
  unset host_ld_dir
  unset host_ld

  if [ x"$CROSSROAD_PLATFORM" = x"w32" ] && [ -d "$CROSSROAD_CUSTOM_MINGW_W32_PREFIX" ]; then
      export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$CROSSROAD_CUSTOM_MINGW_W32_PREFIX/lib32/:$CROSSROAD_CUSTOM_MINGW_W32_PREFIX/lib/"
  fi
  if [ x"$CROSSROAD_PLATFORM" = x"w64" ] && [ -d "$CROSSROAD_CUSTOM_MINGW_W64_PREFIX" ]; then
      export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$CROSSROAD_CUSTOM_MINGW_W64_PREFIX/lib64/:$CROSSROAD_CUSTOM_MINGW_W64_PREFIX/lib/"
  fi
  if [ -d "$CROSSROAD_GUESSED_MINGW_PREFIX" ]; then
      export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$CROSSROAD_GUESSED_MINGW_PREFIX/lib32/:$CROSSROAD_GUESSED_MINGW_PREFIX/lib/"
  fi
fi

if [ x"$CROSSROAD_PLATFORM" = x"native" ]; then
  # Native environment does not need much tweaking nor any additional
  # tools. We only need to add pkg-config path.
  export PKG_CONFIG_PATH=$CROSSROAD_PREFIX/lib/pkgconfig:$CROSSROAD_PREFIX/share/pkgconfig:$PKG_CONFIG_PATH
  if [ "x$CROSSROAD_WORD_SIZE" != "x" ]; then
    export PKG_CONFIG_PATH=$CROSSROAD_PREFIX/lib${CROSSROAD_WORD_SIZE}/pkgconfig:$PKG_CONFIG_PATH
  fi
  if [ "x`$GCC -print-multiarch`" != "x" ]; then
    export PKG_CONFIG_PATH=$CROSSROAD_PREFIX/lib/`$GCC -print-multiarch`/pkgconfig/:$PKG_CONFIG_PATH
  fi

  export CROSSROAD_HOST="$CROSSROAD_BUILD"
else
  # Adding some typical distribution paths.
  # Note: I could also try to guess the user path from `which ${CROSSROAD_HOST}-gcc`.
  # But it may not always work. For instance if the user uses ccache.
  export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/usr/local/$CROSSROAD_HOST/lib${CROSSROAD_WORD_SIZE}/:/usr/local/$CROSSROAD_HOST/lib"
  export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/usr/$CROSSROAD_HOST/lib${CROSSROAD_WORD_SIZE}/:/usr/$CROSSROAD_HOST/lib/"

  export PATH="@DATADIR@/share/crossroad/bin:$PATH"
fi

mkdir -p $CROSSROAD_PREFIX/bin
# no such file or directory error on non-existing aclocal.
mkdir -p $CROSSROAD_PREFIX/share/aclocal
export ACLOCAL_FLAGS="-I$CROSSROAD_PREFIX/share/aclocal"
# no such file or directory warning on non-existing include.
mkdir -p $CROSSROAD_PREFIX/include
mkdir -p $CROSSROAD_PREFIX/lib

# So that the system-wide python can still find any locale lib.
# While Python scripts should theoretically be platform-agnostic, they
# may depend on host-compiled libraries. So let's only edit this
# variable for native platform. To add a library to be used on the build
# environment, install is in a tool dependency project.
if [ x"$CROSSROAD_PLATFORM" = x"native" ]; then
  for dir in $(find $CROSSROAD_PREFIX/lib/ $CROSSROAD_PREFIX/lib${CROSSROAD_WORD_SIZE}/ -maxdepth 1 -name 'python3.*' 2>/dev/null);
  do
    export PYTHONPATH=${dir}/site-packages:$PYTHONPATH
  done;
  # For gdbus-codegen.
  export PYTHONPATH=$CROSSROAD_PREFIX/share/glib-2.0/:$PYTHONPATH
fi

# g-ir-scanner looks in XDG_DATA_DIRS/gir-1.0
# If XDG_DATA_DIRS is set, many software won't add the default values
# (i.e. "/usr/local/share/:/usr/share/") which breaks some native tools
# (for instance glib's g_content_type_guess() will fail mime types
# guesses). Since cross-platform GObject Introspection won't work
# properly yet, I temporarily disable this env value setting.
if [ -n "$DEPS_XDG_DATA_DIRS" ]; then
  export XDG_DATA_DIRS="$CROSSROAD_PREFIX/share:$DEPS_XDG_DATA_DIRS:$XDG_DATA_DIRS:/usr/local/share/:/usr/share/"
else
  export XDG_DATA_DIRS="$CROSSROAD_PREFIX/share:$XDG_DATA_DIRS:/usr/local/share/:/usr/share/"
fi

ccd() {
  # Yes == 1 / No == -1 / Ask == 0
  yesno=0
  setdir=0
  dir=""
  nooption=0
  for arg in "$@"
  do
    case "$arg" in
      "-y"|"--yes" ) if [ $nooption -eq 0 ]; then
                       yesno=1
                     else
                       setdir=1
                     fi;;
      "-n"|"--no" ) yesno=-1;;
      * ) setdir=1;;
    esac
    if [ $setdir -eq 1 ]; then
      if [ "x$dir" != "x" ]; then
        echo "Usage: ccd: too many arguments '$dir'"
        return 3
      else
        dir="$arg"
      fi
    fi
    setdir=0
  done
  if [ ! -d "$CROSSROAD_HOME/$dir" ]; then
    if [ -a "$CROSSROAD_HOME/$dir" ]; then
      echo "Path $CROSSROAD_HOME/$dir is not a directory";
      return 1;
    else
      if [ $yesno -eq 0 ]; then
        read -p "Folder $CROSSROAD_HOME/$dir does not exist. Do you want to create it? [yN] " answer
        case $answer in
          [yY]* ) yesno=1;;
          * ) yesno=0;;
        esac
      fi
      if [ $yesno -eq 1 ]; then
        mkdir -p "$CROSSROAD_HOME/$dir"
      else
        echo "Directory creation cancelled"
      fi
    fi
  fi
  if [ -d "$CROSSROAD_HOME/$dir" ]; then
    cd -- $CROSSROAD_HOME/$dir
    return 0;
  fi
  return 2;
}
